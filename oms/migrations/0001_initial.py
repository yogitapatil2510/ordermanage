# Generated by Django 2.0.2 on 2021-03-03 16:31

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Purchase',
            fields=[
                ('PO_NO', models.IntegerField(primary_key=True, serialize=False)),
                ('Process', models.CharField(max_length=90)),
                ('ConcernPerson', models.CharField(max_length=90)),
            ],
        ),
    ]
