from django.db import models

# Create your models here.
class Purchase(models.Model):
    PO_NO=models.IntegerField(primary_key=True)
    Process=models.CharField(max_length=90)
    ConcernPerson=models.CharField(max_length=90)